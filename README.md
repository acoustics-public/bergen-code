# KZK Equation solver - The Bergen Code

Original source code as implemented by Prof. Emer. [Jarle Berntsen](https://www.uib.no/en/persons/Jarle.Peder.Berntsen), Department of Mathematics, University of Bergen, Bergen, Norway.

## Literature

- Aanonsen, Barkve, Naze Tjøtta & Tjøtta, *J. Acoust. Soc. Am.*, 75, 749-768 (1984)
- Hamilton, Naze Tjøtta & Tjøtta, *J. Acoust. Soc. Am.*, 78, 202-216 (1985)

## License

We propose CC BY-SA 4.0.